/*
 * Copyright (c) 2014, Fraunhofer Heinrich-Hertz-Institut.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \file
 *         Wireless preloading app.
 * \author
 *         Konrad Krentz <konrad.krentz@gmail.com>
 */

#include "sixdoku.h"
#include "dev/button-sensor.h"
#include "net/netstack.h"
#include "net/packetbuf.h"
#include "net/llsec/command-broker.h"
#include "sys/ctimer.h"
#include "lib/uhash-chain.h"
#include "lib/uhash.h"
#include "lib/prng.h"
#include "lib/aes-128.h"

#define PRESSED_IDENTIFIER  0x20
#define RELEASED_IDENTIFIER 0x21
#define PRESS_IDENTIFIER    0x22
#define RELEASE_IDENTIFIER  0x23
#define BIND_IDENTIFIER     0x24
#define BOUND_IDENTIFIER    0x25
#define PING_IDENTIFIER     0x26
#define PONG_IDENTIFIER     0x27

#define MAX_PINGS           64
#define TIMEOUT             (CLOCK_SECOND/2)
#define INVALID_RSSI        127

#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else /* DEBUG */
#define PRINTF(...)
#endif /* DEBUG */

static void next_phase(void *ptr);
static void send_pong(void);
static void on_sampling_done(void);

PROCESS(sixdoku_process, "Over-the-air preloading");
AUTOSTART_PROCESSES(&sixdoku_process);
static int8_t phase;
static struct ctimer timeout;
static uint8_t current_channel;
static int8_t rssis[MAX_PINGS];
static uhash_chain_t pair_chain;
static uhash_chain_t pong_chain;
static linkaddr_t mother_duck_addr;
static uint8_t mother_duck_position;
static uint8_t mother_duck_value[UHASH_CHAIN_VALUE_LEN];
static uint8_t mother_duck_ping_position;
static uint8_t mother_duck_ping_value[UHASH_CHAIN_VALUE_LEN];

/* All the rest is for simulating Wilhelm's scheme */
#define DEBUG1_IDENTIFIER   0x28
#define DEBUG2_IDENTIFIER   0x29
#define DISTANCE            14 /* in dBm x 2 */

static void center_and_scale(int8_t *rssis);
static void on_debug1(void);
static void on_debug2(void);
static void quantize_rssis_and_get_actions(int8_t *rssis, int8_t *actions);
static int8_t quantize_rssi_and_get_action(int8_t *rssi);
static void amplify(uint8_t *quantized_rssis);
static int8_t alice_rssis[MAX_PINGS];

/*---------------------------------------------------------------------------*/
static void
hop_channel(void)
{
  current_channel = ((current_channel - 11 + 7) % 16) + 11;
}
/*---------------------------------------------------------------------------*/
static void
print_rssis(char *preamble,int8_t *rssis)
{
  uint8_t i;

  current_channel = CC2420_CONF_CHANNEL;
  for(i = 0; i < MAX_PINGS; i++) {
    hop_channel();
    PRINTF("%s;0;%i;%i;%i;0\n",
        preamble,
        current_channel,
        i,
        rssis[i]);
  }
}
/*---------------------------------------------------------------------------*/
static uint8_t *
prepare_command(uint8_t command_frame_id)
{
  uint8_t *payload;
  
  packetbuf_clear();
  packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_CMDFRAME);
  payload = packetbuf_dataptr();
  payload[0] = command_frame_id;
  packetbuf_set_datalen(1);
  
  return payload + 1;
}
/*---------------------------------------------------------------------------*/
static uint8_t *
send_pressed_released(uint8_t command_frame_id)
{
  uint8_t *payload;
  uint8_t offset;

  if(!uhash_chain_has_next(&pair_chain)) {
    return NULL;
  }

  offset = 0;
  payload = prepare_command(command_frame_id);
  payload[offset++] = pair_chain.current_position;
  memcpy(payload + offset, pair_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  offset += UHASH_CHAIN_VALUE_LEN;
  if(!linkaddr_cmp(&mother_duck_addr, &linkaddr_null)) {
    payload[offset++] = mother_duck_position;
    memcpy(payload + offset, mother_duck_value, UHASH_CHAIN_VALUE_LEN);
    offset += UHASH_CHAIN_VALUE_LEN;
  }
  packetbuf_set_datalen(1 + offset);
  NETSTACK_MAC.send(NULL, NULL);
  uhash_chain_next(&pair_chain);
}
/*---------------------------------------------------------------------------*/
static int
is_valid_press_or_release(void)
{
  uint8_t *payload;
  uint8_t value[UHASH_CHAIN_VALUE_LEN];
  uint8_t gap;
  
  if(packetbuf_holds_broadcast()) {
    return 0;
  }

  payload = packetbuf_dataptr();

  if(linkaddr_cmp(&mother_duck_addr, &linkaddr_null)) {
    linkaddr_copy(&mother_duck_addr, packetbuf_addr(PACKETBUF_ADDR_SENDER));
    mother_duck_position = payload[1];
    memcpy(mother_duck_value, payload + 2, UHASH_CHAIN_VALUE_LEN);
    PRINTF("6doku: Got mother duck\n");
    return 1;
  }
  
  if(!linkaddr_cmp(&mother_duck_addr, packetbuf_addr(PACKETBUF_ADDR_SENDER))) {
    PRINTF("6doku: Not my mother\n");
    return 0;
  }

  if(mother_duck_position >= payload[1]) {
    PRINTF("6doku: Too low\n");
    return 0;
  }

  memcpy(value, payload + 2, UHASH_CHAIN_VALUE_LEN);
  gap = payload[1] - mother_duck_position;
  while(gap--) {
    uhash_l_to_l(value, value, UHASH_CHAIN_VALUE_LEN);
  }
  
  if(memcmp(value, mother_duck_value, UHASH_CHAIN_VALUE_LEN)) {
    PRINTF("6doku: Not authentic %i %02X%02X%02X%02X\n", payload[1], payload[2], payload[3], payload[4], payload[5]);
    return 0;
  }
  
  PRINTF("6doku: Authentic\n");
  
  mother_duck_position = payload[1];
  memcpy(mother_duck_value, payload + 2, UHASH_CHAIN_VALUE_LEN);
  
  return 1;
}
/*---------------------------------------------------------------------------*/
static void
on_press(void)
{
  if(is_valid_press_or_release()) {
    send_pressed_released(PRESSED_IDENTIFIER);
  }
}
/*---------------------------------------------------------------------------*/
static void
on_release(void)
{
  if(is_valid_press_or_release()) {
    send_pressed_released(RELEASED_IDENTIFIER);
  }
}
/*---------------------------------------------------------------------------*/
static void
on_bound_sent(void *ptr, int status, int transmissions)
{
  next_phase(NULL);
}
/*---------------------------------------------------------------------------*/
static void
on_bind(void)
{
  uint8_t *payload;
  uint8_t value[UHASH_CHAIN_VALUE_LEN];
  uint8_t gap;

  if(!linkaddr_cmp(&mother_duck_addr, packetbuf_addr(PACKETBUF_ADDR_SENDER))) {
    PRINTF("6doku: Not my mother\n");
    return;
  }
  
  payload = packetbuf_dataptr();
  memcpy(value, payload + 1, UHASH_CHAIN_VALUE_LEN);
  uhash_l_to_l(value, value, UHASH_CHAIN_VALUE_LEN);
  gap = UHASH_CHAIN_LEN - mother_duck_position;
  while(gap--) {
    uhash_l_to_l(value, value, UHASH_CHAIN_VALUE_LEN);
  }
  
  if(memcmp(value, mother_duck_value, UHASH_CHAIN_VALUE_LEN)) {
    PRINTF("6doku: Not Authentic bind\n");
    return;
  }
  
  command_broker_unsubscribe(on_press);
  command_broker_unsubscribe(on_release);
  if(!command_broker_subscribe(DEBUG1_IDENTIFIER, on_debug1)
      || !command_broker_subscribe(DEBUG2_IDENTIFIER, on_debug2)) {
    PRINTF("ERROR\n");
  }
  
  mother_duck_ping_position = 0;
  memcpy(mother_duck_ping_value, payload + 1, UHASH_CHAIN_VALUE_LEN);
  phase = -1;
  current_channel = 26;
  payload = prepare_command(BOUND_IDENTIFIER);
  memcpy(payload, pong_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, &mother_duck_addr);
  packetbuf_set_datalen(1 + UHASH_CHAIN_VALUE_LEN);
  NETSTACK_MAC.send(on_bound_sent, NULL);
}
/*---------------------------------------------------------------------------*/ 
static void
set_radio(uint8_t channel, int8_t dbm)
{
  NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, channel);
  NETSTACK_RADIO.set_value(RADIO_PARAM_TXPOWER, dbm);
}
/*---------------------------------------------------------------------------*/
static void
on_timeout(void *ptr)
{
  rssis[phase] = INVALID_RSSI;
  next_phase(&timeout);
}
/*---------------------------------------------------------------------------*/
static void
next_phase(void *ptr)
{
  if(++phase == MAX_PINGS) {
    on_sampling_done();
    return;
  }
  
  if(ptr) {
    ctimer_reset(&timeout);
  } else {
    ctimer_stop(&timeout);
    ctimer_set(&timeout, TIMEOUT, on_timeout, NULL);
  }
  
  hop_channel();
  uhash_chain_next(&pong_chain);
  set_radio(current_channel, -15);
}
/*---------------------------------------------------------------------------*/
static void
on_ping(void)
{
  uint8_t *payload;
  uint8_t value[UHASH_CHAIN_VALUE_LEN];
  uint8_t gap;
  uint8_t pos;
  
  /* TODO avoid duplicate code in on_ping/on_bind/... */
  payload = packetbuf_dataptr();
  pos = phase + 1;
  memcpy(value, payload + 1, UHASH_CHAIN_VALUE_LEN);
  gap = pos - mother_duck_ping_position;
  while(gap--) {
    uhash_l_to_l(value, value, UHASH_CHAIN_VALUE_LEN);
  }
  
  if(memcmp(value, mother_duck_ping_value, UHASH_CHAIN_VALUE_LEN)) {
    return;
  }

  memcpy(mother_duck_ping_value, payload + 1, UHASH_CHAIN_VALUE_LEN);
  mother_duck_ping_position = pos;
  rssis[phase] = packetbuf_attr(PACKETBUF_ATTR_RSSI);
  send_pong();
  next_phase(NULL);
}
/*---------------------------------------------------------------------------*/
static void
send_pong(void)
{
  uint8_t *payload;

  packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, &mother_duck_addr);
  payload = prepare_command(PONG_IDENTIFIER);
  memcpy(payload, pong_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  packetbuf_set_datalen(1 + UHASH_CHAIN_VALUE_LEN);
  NETSTACK_MAC.send(NULL, NULL);
}
/*---------------------------------------------------------------------------*/
static void
on_sampling_done(void)
{
  ctimer_stop(&timeout);
  set_radio(26, 0);
  print_rssis("Bob;PING", rssis);
}
/*---------------------------------------------------------------------------*/
static void
on_debug1(void)
{
  memcpy(alice_rssis, ((int8_t *) packetbuf_dataptr()) + 1, 32);
  current_channel = 26;
}
/*---------------------------------------------------------------------------*/
static void
on_debug2(void)
{
  int8_t actions[MAX_PINGS];
  uint8_t i;
  
  memcpy(alice_rssis + 32, ((int8_t *) packetbuf_dataptr()) + 1, 32);
  
  print_rssis("Alice;PONG", alice_rssis);
  
  /* simulate Wilhelm's scheme for now */
  PRINTF("6doku: A centers and scales RSSIs\n");
  center_and_scale(alice_rssis);
  print_rssis("Alice;PONG", alice_rssis);
  PRINTF("6doku: A quantizes\n");
  quantize_rssis_and_get_actions(alice_rssis, actions);
  print_rssis("Alice;PONG", alice_rssis);
  PRINTF("6doku: A amplifies\n");
  amplify((uint8_t *) alice_rssis);
  PRINTF("6doku: A sends actions over to B using Jane DoS\n");
  
  PRINTF("6doku: B removes missed PONGs\n");
  for(i = 0; i < MAX_PINGS; i++) {
    rssis[i] = (alice_rssis[i] == INVALID_RSSI) ? INVALID_RSSI : rssis[i];
  }
  PRINTF("6doku: B centers and scales RSSIs\n");
  center_and_scale(rssis);
  print_rssis("Bob;PING", rssis);
  PRINTF("6doku: B quantizes\n");
  for(i = 0; i < MAX_PINGS; i++) {
    if(rssis[i] != INVALID_RSSI) {
      rssis[i] += actions[i];
      quantize_rssi_and_get_action(rssis + i);
    }
  }
  print_rssis("Bob;PING", rssis);
  PRINTF("6doku: B amplifies\n");
  amplify((uint8_t *) rssis);
}
/*---------------------------------------------------------------------------*/
static void
generate_hash_tree(void)
{
#if DEBUG
  struct uhash_chain_pebble *pong_root;
#endif /* DEBUG */
  prng_rand(pong_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  uhash_chain_init(&pong_chain);
  
#if DEBUG
  pong_root = list_tail(pong_chain.pebble_list);

  PRINTF("6doku: pong root: %02X%02X%02X%02X\n",
        pong_root->value[0],
        pong_root->value[1],
        pong_root->value[2],
        pong_root->value[3]);

  PRINTF("6doku: pong ending: %02X%02X%02X%02X\n",
      pong_chain.current_value[0],
      pong_chain.current_value[1],
      pong_chain.current_value[2],
      pong_chain.current_value[3]);
#endif /* DEBUG */

  memcpy(pair_chain.current_value, pong_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  uhash_l_to_l(pair_chain.current_value, pair_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  uhash_chain_init(&pair_chain);
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sixdoku_process, ev, data)
{
  PROCESS_BEGIN();
  
  SENSORS_ACTIVATE(button_sensor);
  generate_hash_tree();
  linkaddr_copy(&mother_duck_addr, &linkaddr_null);
  
  if(command_broker_subscribe(PRESS_IDENTIFIER, on_press)
      && command_broker_subscribe(RELEASE_IDENTIFIER, on_release)
      && command_broker_subscribe(BIND_IDENTIFIER, on_bind)
      && command_broker_subscribe(PING_IDENTIFIER, on_ping)) {
    while(1) {
      PROCESS_WAIT_EVENT();
      if((ev == sensors_event) && (data == &button_sensor)) {
        PRINTF("6doku: BUTTON value %i\n", button_sensor.value(0));
        send_pressed_released(
            button_sensor.value(0) ? PRESSED_IDENTIFIER : RELEASED_IDENTIFIER);
      }
    }
  } else {
    PRINTF("6doku: ERROR\n");
  }
  
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
static void
center_and_scale(int8_t *rssis)
{
  uint8_t i;
  int16_t mean;
  uint8_t count;
  
  mean = 0;
  count = 0;
  for(i = 0; i < MAX_PINGS; i++) {
    if(rssis[i] != INVALID_RSSI) {
      mean += rssis[i] * 2;
      count++;
    }
  }
  mean /= count;
  PRINTF("mean = %i\n", mean);
  
  for(i = 0; i < MAX_PINGS; i++) {
    if(rssis[i] != INVALID_RSSI) {
      rssis[i] = 2 * rssis[i] - mean;
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
quantize_rssis_and_get_actions(int8_t *rssis, int8_t *actions)
{
  uint8_t i;
  
  for(i = 0; i < MAX_PINGS; i++) {
    if(rssis[i] != INVALID_RSSI) {
      actions[i] = quantize_rssi_and_get_action(rssis + i);
    } else {
      actions[i] = INVALID_RSSI;
    }
  }
}
/*---------------------------------------------------------------------------*/
static int8_t
quantize_rssi_and_get_action(int8_t *rssi)
{
  int8_t remainder;
  int8_t action;
  
  remainder = *rssi % DISTANCE;
  if((remainder >= (DISTANCE/-2)) && (remainder <= (DISTANCE/2))) {
    action = -remainder;
  } else {
    if(remainder > (DISTANCE/2)) {
      action = DISTANCE - remainder;
    } else {
      action = -DISTANCE - remainder;
    }
  }
  
  *rssi += action;
  return action;
}
/*---------------------------------------------------------------------------*/
static void
amplify(uint8_t *quantized_rssis)
{
  uint8_t key[AES_128_KEY_LENGTH];
  uint8_t i;
  uint8_t j;
  
  memset(key, 0, AES_128_KEY_LENGTH);
  AES_128.set_key(key);
  for(i = 0; i < (MAX_PINGS / AES_128_BLOCK_SIZE); i++) {
    for(j = 0; j < AES_128_BLOCK_SIZE; j++) {
      key[j] ^= quantized_rssis[(i * AES_128_BLOCK_SIZE) + j];
    }
    AES_128.encrypt(key);
  }
#if DEBUG
  for(i = 0; i < AES_128_KEY_LENGTH; i++) {
    PRINTF("%02X", key[i]);
  }
  PRINTF("\n");
#endif /* DEBUG */
}
/*---------------------------------------------------------------------------*/

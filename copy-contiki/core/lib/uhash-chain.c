/*
 * Copyright (c) 2013, Fraunhofer Heinrich-Hertz-Institut.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Lightweight hash chain.
 * \author
 *         Konrad Krentz <konrad.krentz@gmail.com>
 */

#include "lib/uhash.h"
#include "lib/uhash-chain.h"
#include <string.h>

#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else /* DEBUG */
#define PRINTF(...)
#endif /* DEBUG */

/*---------------------------------------------------------------------------*/
void
uhash_chain_init(uhash_chain_t *chain)
{
  uint8_t j;
  uint16_t i;
  
  LIST_STRUCT_INIT(chain, pebble_list);
  for(j = 0; j < UHASH_CHAIN_SIGMA; j++) {
    chain->pebbles[j].start_incr = 3 * (2 << j);
    chain->pebbles[j].dest_incr = 2 << (j + 1);
    chain->pebbles[j].position = chain->pebbles[j].destination = 2 << j;
    list_add(chain->pebble_list, &chain->pebbles[j]);
    PRINTF("uhash-chain: Pebble at %i start_incr=%i dest_incr=%i\n", chain->pebbles[j].position, chain->pebbles[j].start_incr, chain->pebbles[j].dest_incr);
  }
  
  PRINTF("uhash-chain: Creating chain\n");
  j = UHASH_CHAIN_SIGMA - 1;
  i = chain->pebbles[j].position;

  while(i) {
    if((j >= 0) && (i == chain->pebbles[j].position)) {
      PRINTF("uhash-chain: Placing pebble at %i\n", i);
      memcpy(chain->pebbles[j].value, chain->current_value, UHASH_CHAIN_VALUE_LEN);
      j--;
    }
    
    PRINTF("uhash-chain: %i %02X%02X%02X%02X\n",
        i,
        chain->current_value[0],
        chain->current_value[1],
        chain->current_value[2],
        chain->current_value[3]);
    uhash_l_to_l(chain->current_value, chain->current_value, UHASH_CHAIN_VALUE_LEN);
    i--;
  }
  PRINTF("uhash-chain: %i %02X%02X%02X%02X\n",
      i,
      chain->current_value[0],
      chain->current_value[1],
      chain->current_value[2],
      chain->current_value[3]);
  chain->current_position = 0;
}
/*---------------------------------------------------------------------------*/
int
uhash_chain_next(uhash_chain_t *chain)
{
  struct uhash_chain_pebble *head;
  struct uhash_chain_pebble *next;
  struct uhash_chain_pebble *prev;
  
  if(chain->current_position == UHASH_CHAIN_LEN) {
    return 0;
  }
  chain->current_position++;
  
  head = next = list_head(chain->pebble_list);
  while(next) {
    if(next->position != next->destination) {
      next->position -= 2;
      uhash_l_to_l(next->value, next->value, UHASH_CHAIN_VALUE_LEN);
      uhash_l_to_l(next->value, next->value, UHASH_CHAIN_VALUE_LEN);
    }
    next = list_item_next(next);
  }
  
  if(chain->current_position & 1) {
    uhash_l_to_l(chain->current_value, head->value, UHASH_CHAIN_VALUE_LEN);
  } else {
    memcpy(chain->current_value, head->value, UHASH_CHAIN_VALUE_LEN);
    head->destination = head->position + head->dest_incr;
    head->position += head->start_incr;
    if(head->destination > UHASH_CHAIN_LEN) {
      head->destination = 0xFF;
      head->position = 0xFF;
    } else {
      next = list_item_next(head);
      while(head->position != next->position) {
        next = list_item_next(next);
      }
      memcpy(head->value, next->value, UHASH_CHAIN_VALUE_LEN);
    }
  }
  
  next = list_item_next(head);
  if(head->destination > next->destination) {
    list_remove(chain->pebble_list, head);
    while(next && (head->destination > next->destination)) {
      prev = next;
      next = list_item_next(next);
    }
    list_insert(chain->pebble_list, prev, head);
  }
  
  return 1;
}
/*---------------------------------------------------------------------------*/
int
uhash_chain_has_next(uhash_chain_t *chain)
{
  return chain->current_position != UHASH_CHAIN_LEN;
}
/*---------------------------------------------------------------------------*/

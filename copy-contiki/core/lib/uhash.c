/*
 * Copyright (c) 2013, Fraunhofer Heinrich-Hertz-Institut.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Lightweight one-way function using Davies-Meyer.
 * \author
 *         Konrad Krentz <konrad.krentz@gmail.com>
 */

#include "lib/uhash.h"
#include "lib/aes-128.h"
#include <string.h>

/*---------------------------------------------------------------------------*/
void
uhash_16_to_16(uint8_t *hash, uint8_t *msg)
{
  memset(hash, 0, 16);
  
  AES_128.set_key(msg);
  AES_128.encrypt(hash);
}
/*---------------------------------------------------------------------------*/
void
uhash_l_to_l(uint8_t *hash, uint8_t *msg, uint8_t l)
{
  uint8_t hash_temp[16];
  uint8_t msg_temp[16];
  
  memcpy(msg_temp, msg, l);
  memset(msg_temp + l, 0, 16 - l);
  uhash_16_to_16(hash_temp, msg_temp);
  memcpy(hash, hash_temp, l);
}
/*---------------------------------------------------------------------------*/

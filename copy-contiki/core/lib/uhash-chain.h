/*
 * Copyright (c) 2013, Fraunhofer Heinrich-Hertz-Institut.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Lightweight hash chain.
 * \author
 *         Konrad Krentz <konrad.krentz@gmail.com>
 */

#ifndef UHASH_CHAIN_H_
#define UHASH_CHAIN_H_

#include "contiki-conf.h"
#include "lib/list.h"

#define UHASH_CHAIN_VALUE_LEN 4
#define UHASH_CHAIN_LEN       128
#define UHASH_CHAIN_SIGMA     7

struct uhash_chain_pebble {
  struct uhash_chain_pebble *next;

  uint16_t start_incr;
  uint16_t dest_incr;
  uint16_t position;
  uint16_t destination;
  uint8_t value[UHASH_CHAIN_VALUE_LEN];
};

typedef struct {
  LIST_STRUCT(pebble_list);
  struct uhash_chain_pebble pebbles[UHASH_CHAIN_SIGMA];
  uint8_t current_position;
  uint8_t current_value[UHASH_CHAIN_VALUE_LEN];
} uhash_chain_t;

/**
 * \brief    Generates a new hash chain.
 */
void uhash_chain_init(uhash_chain_t *chain);

/**
 * \brief    Restores the next preimage.
 * \retval 0 Error
 */
int uhash_chain_next(uhash_chain_t *chain);

/**
 * \brief    Whether a change is used up
 */
int uhash_chain_has_next(uhash_chain_t *chain);

#endif /* UHASH_CHAIN_H_ */

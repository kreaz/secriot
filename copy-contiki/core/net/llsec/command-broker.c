/*
 * Copyright (c) 2014, Fraunhofer Heinrich-Hertz-Institut.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Forwards commmand frames to subscribers.
 * \author
 *         Konrad Krentz <konrad.krentz@gmail.com>
 */

#include "net/llsec/command-broker.h"
#include "net/packetbuf.h"
#include "lib/memb.h"
#include "lib/list.h"

#define MAX_SUBSCRIBERS 4

struct subscriber {
  struct subscriber *next;
  uint8_t command_frame_id;
  command_broker_notify_t callback;
};

MEMB(subscribers_memb, struct subscriber, MAX_SUBSCRIBERS);
LIST(subscribers_list);

/*---------------------------------------------------------------------------*/
int
command_broker_subscribe(uint8_t command_frame_id, command_broker_notify_t callback)
{
  struct subscriber *subscriber;
  
  subscriber = memb_alloc(&subscribers_memb);
  if(!subscriber) {
    return 0;
  }
  subscriber->command_frame_id = command_frame_id;
  subscriber->callback = callback;
  list_add(subscribers_list, subscriber);
  return 1;
}
/*---------------------------------------------------------------------------*/
void
command_broker_unsubscribe(command_broker_notify_t callback)
{
  struct subscriber *subscriber;
  
  subscriber = list_head(subscribers_list);
  while(subscriber) {
    if(subscriber->callback == callback) {
      list_remove(subscribers_list, subscriber);
      memb_free(&subscribers_memb, subscriber);
      return;
    }
    subscriber = list_item_next(subscriber);
  }
}
/*---------------------------------------------------------------------------*/
void
command_broker_publish(void)
{
  struct subscriber *subscriber;
  uint8_t *payload;
  uint8_t command_frame_id;
  
  payload = (uint8_t *) packetbuf_dataptr();
  command_frame_id = payload[0];
  
  subscriber = list_head(subscribers_list);
  while(subscriber) {
    if(subscriber->command_frame_id == command_frame_id) {
      subscriber->callback();
      return;
    }
    subscriber = list_item_next(subscriber);
  }
}
/*---------------------------------------------------------------------------*/
void
command_broker_init(void)
{
  memb_init(&subscribers_memb);
  list_init(subscribers_list);
}
/*---------------------------------------------------------------------------*/

package de.hhi.sixdoku.services.channels;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * Holds the noise in dBm of all 802.15.4 channels in the 2.4GHz band.
 */
public class EnergyScanResult extends LinkedHashMap<Integer, Byte> implements Iterable<Integer> {

    public EnergyScanResult() {
        super(16);
    }

    /**
     * Retains the best {@code num} channels.
     */
    public void retainBestChannels(int num) {
        Integer worstChannel;

        while (size() > num) {
            worstChannel = null;

            for (Integer channel : keySet()) {
                if (worstChannel == null) {
                    worstChannel = channel;
                } else if (get(worstChannel) < get(channel)) {
                    worstChannel = channel;
                }
            }

            remove(worstChannel);
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return new YaoChannelIterator(this);
    }
}

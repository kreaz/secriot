package de.hhi.sixdoku.services.crypt;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import de.hhi.sixdoku.ui.MainActivity;

public class HashChain {

    public static final int VALUE_LENGTH = 4;
    public static final int CHAIN_LEN = 128;
    public static final int CHAIN_SIGMA = 7;

    private DaviesMeyer daviesMeyer;
    public List<Pebble> pebbles;
    public int currentPosition;
    public byte[] currentValue;
    private byte[] root;

    public HashChain(byte[] root) {
        int j;
        Pebble pebble;
        int i;

        this.root = root;
        currentValue = root;
        daviesMeyer = new DaviesMeyer();
        pebbles = new LinkedList<>();
        for (j = 0; j < CHAIN_SIGMA; j++) {
            pebble = new Pebble();
            pebble.startIncr = 3 * (2 << j);
            pebble.destIncr = 2 << (j + 1);
            pebble.position = pebble.destination = 2 << j;
            pebbles.add(pebble);
            Log.d(MainActivity.TAG, "Pebble at " + pebble.position + " " + pebble.startIncr + " " + pebble.destIncr);
        }

        j = CHAIN_SIGMA - 1;
        i = pebbles.get(j).position;

        while (i != 0) {
            if ((j >= 0) && (i == pebbles.get(j).position)) {
                Log.d(MainActivity.TAG, "Placing pebble at " + i);
                pebbles.get(j).value = currentValue;
                j--;
            }

            printCurrentValue();
            currentValue = daviesMeyer.hashltol(currentValue, VALUE_LENGTH);
            i--;
        }
        printCurrentValue();
    }

    public void printCurrentValue() {
        StringBuilder sb;

        sb = new StringBuilder(VALUE_LENGTH * 2);

        for (byte b : currentValue) {
            sb.append(String.format("%02x", b));
        }
        Log.d(MainActivity.TAG, sb.toString());
    }

    public boolean next() {
        Pebble head;
        Pebble next;
        int i;

        if (currentPosition == CHAIN_LEN) {
            return false;
        }
        currentPosition++;

        for (Pebble pebble : pebbles) {
            if (pebble.position != pebble.destination) {
                pebble.position -= 2;
                pebble.value = daviesMeyer.hashltol(pebble.value, VALUE_LENGTH);
                pebble.value = daviesMeyer.hashltol(pebble.value, VALUE_LENGTH);
            }
        }

        head = pebbles.get(0);
        if((currentPosition % 2) != 0) {
            currentValue = daviesMeyer.hashltol(head.value, VALUE_LENGTH);
        } else {
            currentValue = head.value;
            head.destination = head.position + head.destIncr;
            head.position += head.startIncr;
            if (head.destination > CHAIN_LEN) {
                head.destination = 0xFF;
                head.position = 0xFF;
            } else {
                for (Pebble pebble : pebbles) {
                  if (pebble == head) {
                      continue;
                  }

                  if (head.position == pebble.position) {
                      head.value = pebble.value;
                      break;
                  }
                }
            }
        }

        next = pebbles.get(1);
        if (head.destination > next.destination) {
            pebbles.remove(0);

            i = 0;
            while ((i < pebbles.size()) && (head.destination > pebbles.get(i).destination)) {
                i++;
            }

            pebbles.add(i, head);
        }

        return true;
    }

    public byte[] getRoot() {
        Log.d(MainActivity.TAG, "root");
        return root;
    }
}

package de.hhi.sixdoku.libs.ieee154;

import android.util.Log;

import java.nio.ByteBuffer;

import de.hhi.sixdoku.services.dongle.Dongle;
import de.hhi.sixdoku.ui.MainActivity;

public class FrameFactory {

    private byte sequenceNumber;
    private Dongle dongle;

    public FrameFactory(Dongle dongle) {
        this.dongle = dongle;
    }

    public Frame parse(ByteBuffer byteBuffer, Byte rssi) {
        Frame frame = new Frame();
        int i;
        byte b;

        frame.rssi = rssi;
        byteBuffer.reset();

        // Frame Type | Security Enabled | (Frame Pending) | Ack Request | PAN ID Compression
        b = byteBuffer.get();
        frame.frameType = FrameType.getFrameType((byte) (b & 7));
        frame.securityEnabled = (b & (1 << 3)) != 0;
        frame.framePending = (b & (1 << 4)) != 0;
        frame.ackRequested = (b & (1 << 5)) != 0;
        frame.panIdCompressed = (b & (1 << 6)) != 0;

        // Destination Addressing Mode | Frame Version | Source Addressing Mode
        b = byteBuffer.get();
        frame.destinationAddressingMode = AddressingMode.get((byte) ((b >> 2) & 3));
        frame.sourceAddressingMode = AddressingMode.get((byte) ((b >> 6) & 3));

        // Sequence Number
        frame.sequenceNumber = byteBuffer.get();

        // Destination
        if (frame.destinationAddressingMode != AddressingMode.NOT_PRESENT) {
            // PAN ID
            byteBuffer.get();
            byteBuffer.get();

            frame.destination = new byte[frame.destinationAddressingMode.addressLength];

            for (i = 0; i < frame.destination.length; i++) {
                frame.destination[frame.destination.length - i - 1] = byteBuffer.get();
            }
        }

        // Source
        if (frame.sourceAddressingMode != AddressingMode.NOT_PRESENT) {
            if (!frame.panIdCompressed) {
                byteBuffer.get();
                byteBuffer.get();
            }

            frame.source = new byte[frame.sourceAddressingMode.addressLength];
            for (i = 0; i < frame.sourceAddressingMode.addressLength; i++) {
                frame.source[frame.sourceAddressingMode.addressLength - i - 1] = byteBuffer.get();
            }
        }

        if (frame.isCommandFrame()) {
            frame.commandFrameIdentifier = byteBuffer.get();
            Log.d(MainActivity.TAG, "Received command frame " + frame.commandFrameIdentifier);
        }

        frame.bytes = byteBuffer;
        frame.payloadPosition = byteBuffer.position() - 1;

        return frame;
    }

    public Frame createCommandFrame(byte[] destination, byte commandFrameIdentifier, byte[] payload) {
        Frame frame = new Frame();

        frame.frameType = FrameType.COMMAND;
        frame.panIdCompressed = true;
        frame.destinationAddressingMode = destination.length == AddressingMode.SHORT.addressLength ? AddressingMode.SHORT : AddressingMode.EXTENDED;
        frame.destination = destination;
        frame.source = dongle.address;
        frame.sourceAddressingMode = AddressingMode.EXTENDED;
        frame.commandFrameIdentifier = commandFrameIdentifier;
        frame.sequenceNumber = sequenceNumber++;

        writeHeaders(frame);
        frame.bytes.put(commandFrameIdentifier);
        frame.bytes.put(payload);
        frame.bytes.limit(frame.bytes.position());

        return frame;
    }

    public void writeHeaders(Frame frame) {
        int i;

        frame.bytes = ByteBuffer.allocate(127);
        frame.bytes.mark();

        // Frame Type | Security Enabled | (Frame Pending) | Ack Request | PAN ID Compression
        frame.bytes.put((byte) (frame.frameType.value
                | (frame.securityEnabled ? 1 << 3 : 0)
                | (frame.framePending ? 1 << 4 : 0)
                | (frame.ackRequested ?  1 << 5 : 0)
                | (frame.panIdCompressed ? 1 << 6 : 0)));

        // Destination Addressing Mode | Frame Version | Source Addressing Mode
        frame.bytes.put((byte) (frame.destinationAddressingMode.value << 2
                | (1 << 4)
                | frame.sourceAddressingMode.value << 6));

        // Sequence Number
        frame.bytes.put(frame.sequenceNumber);

        // Destination
        if (frame.destinationAddressingMode != AddressingMode.NOT_PRESENT) {
            // PAN ID
            frame.bytes.put((byte) 0xFF);
            frame.bytes.put((byte) 0xFF);

            for (i = 0; i < frame.destination.length; i++) {
                frame.bytes.put(frame.destination[frame.destination.length - i - 1]);
            }
        }

        // Destination
        if (frame.sourceAddressingMode != AddressingMode.NOT_PRESENT) {
            // PAN ID
            if (!frame.panIdCompressed) {
                frame.bytes.put((byte) 0xAB);
                frame.bytes.put((byte) 0xCD);
            }

            for (i = 0; i < frame.source.length; i++) {
                frame.bytes.put(frame.source[frame.source.length - i - 1]);
            }
        }
    }
}

package de.hhi.sixdoku.ui.ducklings.states;

import android.util.Log;

import java.util.Arrays;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.libs.ieee154.Ieee154Constants;
import de.hhi.sixdoku.services.crypt.HashChain;
import de.hhi.sixdoku.ui.MainActivity;
import de.hhi.sixdoku.ui.ducklings.Duckling;

public class BindingState extends DucklingState {

    private byte[] ducklingValue;
    private int ducklingPosition;

    public BindingState(Duckling duckling, byte[] ducklingValue, int ducklingPosition) {
        super(duckling, 2);

        this.ducklingValue = ducklingValue;
        this.ducklingPosition = ducklingPosition;
    }

    @Override
    public void enter() {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#enter");
        /* TODO retransmit */
        duckling.motherDuck.sendCommandFrame(Ieee154Constants.BIND_IDENTIFIER, duckling, duckling.hashTree.pingChain.currentValue);
    }

    @Override
    public boolean onCommandFrame(Frame frame) {
        byte[] value;
        int i;
        int gap;
        byte[] copy;

        if (frame.commandFrameIdentifier != Ieee154Constants.BOUND_IDENTIFIER) {
            return false;
        }

        frame.bytes.position(frame.payloadPosition);
        frame.bytes.get();
        if (frame.bytes.remaining() != HashChain.VALUE_LENGTH) {
            return false;
        }

        value = new byte[HashChain.VALUE_LENGTH];
        for (i = 0; i < HashChain.VALUE_LENGTH; i++) {
            value[i] = frame.bytes.get();
        }
        copy = Arrays.copyOf(value, HashChain.VALUE_LENGTH);
        copy = daviesMeyer.hashltol(copy, HashChain.VALUE_LENGTH);

        gap = HashChain.CHAIN_LEN - ducklingPosition;
        while (gap != 0) {
            copy = daviesMeyer.hashltol(copy, HashChain.VALUE_LENGTH);
            gap--;
        }

        if (!Arrays.equals(copy, ducklingValue)) {
            Log.d(MainActivity.TAG, "Not equal");


            return false;
        }

        duckling.hashTree.pingChain.next();
        duckling.setState(new SamplingState(duckling, value));

        return true;
    }
}

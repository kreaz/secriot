package de.hhi.sixdoku.libs.usb;

import java.io.IOException;

public interface UsbDriver {

    void open() throws IOException;

    boolean isOpen();

    void close();

    int read(byte[] bytes, int offset, int length) throws IOException;

    void write(byte[] bytes, int offset, int length) throws IOException;

    void write(byte[] bytes) throws IOException;

    void doTask(Runnable task);
}

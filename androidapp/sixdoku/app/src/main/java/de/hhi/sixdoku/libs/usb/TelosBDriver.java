package de.hhi.sixdoku.libs.usb;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbManager;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.hhi.sixdoku.ui.MainActivity;

public class TelosBDriver implements UsbDriver {

    public static final int VENDOR_ID = 1027;
    public static final int PRODUCT_ID = 24577;
    private static final int READ_BUFFER_SIZE = 64;
    private static final int WRITE_BUFFER_SIZE = 64;
    private static final int USB_RECIP_DEVICE = 0x00;
    private static final int USB_ENDPOINT_OUT = 0x00;
    private static final int FTDI_DEVICE_OUT_REQTYPE = UsbConstants.USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT;
    private static final int SIO_RESET_REQUEST = 0;
    private static final int SIO_RESET_SIO = 0;
    private static final int SIO_SET_BAUD_RATE_REQUEST = 3;
    private static final int USB_READ_TIMEOUT_MILLIS = 200;
    private static final int USB_WRITE_TIMEOUT_MILLIS = 200;
    private static final int DEFAULT_BAUD_RATE = 115200;
    private static final int MODEM_STATUS_HEADER_LENGTH = 2;
    private UsbDevice usbDevice;
    private UsbDeviceConnection usbConnection;
    private byte[] readBuffer;
    private byte[] writeBuffer;
    private UsbManager usbManager;
    private ExecutorService executor;

    public TelosBDriver(UsbDevice usbDevice, UsbManager usbManager) {
        this.usbDevice = usbDevice;
        this.usbManager = usbManager;
        readBuffer = new byte[READ_BUFFER_SIZE];
        writeBuffer = new byte[WRITE_BUFFER_SIZE];
        executor = Executors.newSingleThreadExecutor();
    }

    public void open() throws IOException {
        usbConnection = usbManager.openDevice(usbDevice);

        if (!isOpen()) {
            throw new IOException("failed to open connection");
        }

        for (int i = 0; i < usbDevice.getInterfaceCount(); i++) {
            if (usbConnection.claimInterface(usbDevice.getInterface(i), true)) {
                Log.d(MainActivity.TAG, "claimInterface " + i + " SUCCESS");
            } else {
                Log.d(MainActivity.TAG, "claimInterface " + i + " FAIL");
            }
        }

        reset();
        setBaudRate(DEFAULT_BAUD_RATE);

        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#open");
    }

    public boolean isOpen() {
        return usbConnection != null;
    }

    public void close() {
        if (isOpen()) {
            usbConnection.close();
            usbConnection = null;
        }
    }

    private void reset() throws IOException {
        int result = usbConnection.controlTransfer(FTDI_DEVICE_OUT_REQTYPE,
                SIO_RESET_REQUEST,
                SIO_RESET_SIO,
                0,
                null,
                0,
                USB_WRITE_TIMEOUT_MILLIS);

        if (result != 0) {
            throw new IOException("Reset failed: result=" + result);
        }
    }

    private void setBaudRate(int baudRate) throws IOException {
        // TODO account for baudRate parameter
        int result = usbConnection.controlTransfer(FTDI_DEVICE_OUT_REQTYPE,
                SIO_SET_BAUD_RATE_REQUEST,
                26,
                0,
                null,
                0,
                USB_WRITE_TIMEOUT_MILLIS);

        if (result != 0) {
            throw new IOException("Setting baudrate failed: result=" + result);
        }
    }

    public int read(byte[] bytes, int offset, int length) throws IOException {
        UsbEndpoint endpoint = usbDevice.getInterface(0).getEndpoint(0);
        int bytesToBeRead = Math.min(Math.min(length + MODEM_STATUS_HEADER_LENGTH, readBuffer.length), endpoint.getMaxPacketSize());
        int actualBytesRead = usbConnection.bulkTransfer(endpoint, readBuffer, bytesToBeRead, USB_READ_TIMEOUT_MILLIS);
        if (actualBytesRead < MODEM_STATUS_HEADER_LENGTH) {
            throw new IOException(actualBytesRead + "Too few bytes read\n");
        }

        System.arraycopy(readBuffer, MODEM_STATUS_HEADER_LENGTH, bytes, offset, actualBytesRead - MODEM_STATUS_HEADER_LENGTH);

        return actualBytesRead - MODEM_STATUS_HEADER_LENGTH;
    }

    public void write(byte[] bytes, int offset, int length) throws IOException {
        UsbEndpoint endpoint = usbDevice.getInterface(0).getEndpoint(1);
        int i;
        int writeBufferOffset;
        int result;

        if (length > writeBuffer.length) {
            throw new IOException("too much\n");
        }

        writeBufferOffset = 0;
        writeBuffer[0] = bytes[offset];
        for (i = 1; i < length - 1; i++) {
            switch (bytes[i + offset]) {
                case 0x00:
                    // encode 0x00 as 0x00 0x00
                    writeBuffer[i + writeBufferOffset++] = (byte) 0x00;
                    writeBuffer[i + writeBufferOffset] = (byte) 0x00;
                    break;
                case 0x0A:
                    // encode line feed as 0x00 0x01
                    writeBuffer[i + writeBufferOffset++] = (byte) 0x00;
                    writeBuffer[i + writeBufferOffset] = (byte) 0x01;
                    break;
                case 0x0D:
                    // encode carriage return as 0x00 0x02
                    writeBuffer[i + writeBufferOffset++] = (byte) 0x00;
                    writeBuffer[i + writeBufferOffset] = (byte) 0x02;
                    break;
                default:
                    writeBuffer[i + writeBufferOffset] = bytes[i + offset];
                    break;
            }
        }
        writeBuffer[writeBufferOffset + length - 1] = bytes[offset + length - 1];

        result = usbConnection.bulkTransfer(endpoint,
                writeBuffer,
                length + writeBufferOffset,
                USB_WRITE_TIMEOUT_MILLIS);

        if (result < 0) {
            throw new IOException("error during bulkTransfer\n");
        }
        if (result < length) {
            // TODO make iterative
            write(writeBuffer, result, length - result);
        }
    }

    public void doTask(Runnable task) {
        executor.submit(task);
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        write(bytes, 0, bytes.length);
    }
}

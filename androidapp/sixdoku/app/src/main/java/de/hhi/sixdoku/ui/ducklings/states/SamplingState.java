package de.hhi.sixdoku.ui.ducklings.states;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.libs.ieee154.Ieee154Constants;
import de.hhi.sixdoku.ui.MainActivity;
import de.hhi.sixdoku.ui.ducklings.Duckling;

public class SamplingState extends DucklingState {

    public static final int MAX_PINGS = 64;
    private byte[] ducklingValue;

    public SamplingState(Duckling duckling, byte[] ducklingValue) {
        super(duckling, 3);

        this.ducklingValue = ducklingValue;
    }

    @Override
    public void enter() {
        if (duckling.motherDuck.isPinging()) {
            Log.e(MainActivity.TAG, "ERROR");
        }
        duckling.motherDuck.ping(duckling.hashTree.pingChain.getRoot(),
                ducklingValue,
                duckling);
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private static String convert(byte[] a) {
        StringWriter sw = new StringWriter();
        PrintWriter wr = new PrintWriter(sw, true);
        for (int i = 0; i < a.length; i++) {
            wr.printf("%d;%d\n", i, a[i]);
        }
        return sw.toString();
    }

    private void dumpToFile(byte[] buffer) {
        if (!isExternalStorageWritable()) {
            Log.e("SAVE DATA", "External storage is not available for write");
            return;
        }
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss_SSS");
        String filename = dateFormat.format(date) + ".rssi";

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, filename);
        try {
            String convertedString = convert(buffer);
            FileOutputStream stream = new FileOutputStream(file, true);
            stream.write(convertedString.getBytes(Charset.forName("UTF-8")));
            stream.close();
            Log.i("SAVE DATA", "Data Saved");
        } catch (IOException e) {
            Log.e("SAVE DATA", "Could not write file " + e.getMessage());
        }
    }

    @Override
    public void onPong(ByteBuffer rssis) {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onPong");

        duckling.motherDuck.sendCommandFrame(Ieee154Constants.DEBUG1_IDENTIFIER, duckling, Arrays.copyOfRange(rssis.array(), 0, 32));
        duckling.motherDuck.sendCommandFrame(Ieee154Constants.DEBUG2_IDENTIFIER, duckling, Arrays.copyOfRange(rssis.array(), 32, 64));

        dumpToFile(rssis.array());
    }

    @Override
    public boolean onCommandFrame(Frame frame) {
        return false;
    }
}

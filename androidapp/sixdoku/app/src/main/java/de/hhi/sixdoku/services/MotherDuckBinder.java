package de.hhi.sixdoku.services;

import android.os.Binder;

public class MotherDuckBinder extends Binder {

    public MotherDuck motherDuck;

    public MotherDuckBinder(MotherDuck motherDuck) {
        this.motherDuck = motherDuck;
    }
}

package de.hhi.sixdoku.ui.ducklings;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.hhi.sixdoku.R;

public class DucklingsAdapter extends ArrayAdapter<Duckling> {

    public Activity activity;
    public DucklingsFragment ducklingsFragment;

    public DucklingsAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_activated_1);
    }

    @Override
    public View getView(final int position, View row, ViewGroup parent) {
        LayoutInflater inflater;
        Duckling duckling;

        if (row == null) {
            inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.row_duckling, parent, false);
        }
        duckling = getItem(position);
        duckling.setRow(row);
        duckling.setPosition(position);

        return row;
    }

    public List<Duckling> get(byte[] address) {
        int i;
        Duckling duckling;
        List<Duckling> ducklings;

        ducklings = new LinkedList<Duckling>();
        for (i = 0; i < getCount(); i++) {
            duckling = getItem(i);
            if (Arrays.equals(duckling.address, address)) {
                ducklings.add(duckling);
            }
        }
        return ducklings;
    }

    public void onUsbChange() {
        if (ducklingsFragment != null) {
            ducklingsFragment.updateEmptyText();
        }
    }
}

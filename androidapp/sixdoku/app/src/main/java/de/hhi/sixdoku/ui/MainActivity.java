package de.hhi.sixdoku.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import de.hhi.sixdoku.R;
import de.hhi.sixdoku.services.MotherDuck;
import de.hhi.sixdoku.services.MotherDuckBinder;
import de.hhi.sixdoku.services.dongle.Dongle;

public class MainActivity extends Activity implements ServiceConnection {

    public static final String TAG = "PRELOAD";
    private MotherDuck motherDuck;

    ///////////////////////// LIFECYCLE //////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerUsbDeviceDetachedReceiver();
        registerUsbPermissionReceiver();
        startMotherDuck();
        bindService(new Intent(this, MotherDuck.class), this, Context.BIND_AUTO_CREATE);
        setContentView(R.layout.main);
    }

    @Override
    protected void onResume() {
        if (motherDuck != null) {
            motherDuck.onUsbChange();
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(usbDeviceDetachedReceiver);
        unregisterReceiver(usbPermissionReceiver);
        unbindService(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        stopMotherDuck();
        super.onBackPressed();
    }

    //////////////////// BROADCAST RECEIVERS /////////////////////

    private BroadcastReceiver usbDeviceDetachedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            motherDuck.onUsbChange();
        }
    };

    private BroadcastReceiver usbPermissionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                Log.d(MainActivity.TAG, "Permission Granted");
                motherDuck.onUsbChange();
            } else {
                Log.d(MainActivity.TAG, "Permission Denied");
            }
        }
    };

    private void registerUsbDeviceDetachedReceiver() {
        registerReceiver(usbDeviceDetachedReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));
    }

    private void registerUsbPermissionReceiver() {
        registerReceiver(usbPermissionReceiver, new IntentFilter(Dongle.ACTION_USB_PERMISSION));
    }

    //////////////////////// Mother Duck ////////////////////////

    private void startMotherDuck() {
        startService(new Intent(this, MotherDuck.class));
    }

    private void stopMotherDuck() {
        stopService(new Intent(this, MotherDuck.class));
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        motherDuck = ((MotherDuckBinder) binder).motherDuck;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        motherDuck = null;
    }

    //////////////////////// OPTIONS MENU ////////////////////////

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear:
                motherDuck.clearDucklings();
                return true;
            default:
                return false;
        }
    }
}

package de.hhi.sixdoku.ui.ducklings.states;

import android.view.MotionEvent;
import android.view.View;

import java.nio.ByteBuffer;
import java.util.Arrays;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.libs.ieee154.Ieee154Constants;
import de.hhi.sixdoku.services.crypt.HashChain;
import de.hhi.sixdoku.services.crypt.HashTree;
import de.hhi.sixdoku.ui.ducklings.Duckling;

public class PairingState extends DucklingState {

    private byte[] ducklingValue;
    private int ducklingPosition;

    public PairingState(Duckling duckling) {
        super(duckling, 0);
    }

    @Override
    public void enter() {
        duckling.setImprintable(false);
        duckling.highlight();
    }

    @Override
    public boolean onCommandFrame(Frame frame) {
        if ((frame.commandFrameIdentifier != Ieee154Constants.PRESSED_IDENTIFIER)
            && (frame.commandFrameIdentifier != Ieee154Constants.RELEASED_IDENTIFIER)) {
            return false;
        }

        if (!isValidPressedReleased(frame)) {
            return false;
        }

        if (frame.commandFrameIdentifier == Ieee154Constants.PRESSED_IDENTIFIER) {
            duckling.highlight();
        } else {
            duckling.unhighlight();
        }

        return true;
    }

    private boolean isValidPressedReleased(Frame frame) {
        int pos;
        byte[] value;
        int i;
        int gap;
        byte[] copy;

        frame.bytes.position(frame.payloadPosition);
        frame.bytes.get();

        if (frame.bytes.remaining() < 1 + HashChain.VALUE_LENGTH) {
            return false;
        }

        pos = frame.bytes.get();
        value = new byte[HashChain.VALUE_LENGTH];
        for (i = 0; i < HashChain.VALUE_LENGTH; i++) {
            value[i] = frame.bytes.get();
        }

        if (ducklingValue == null) {
            ducklingValue = value;
            ducklingPosition = pos;
            return true;
        }

        if (ducklingPosition >= pos) {
            return false;
        }

        copy = Arrays.copyOf(value, value.length);
        gap = pos - ducklingPosition;
        while (gap != 0) {
            copy = daviesMeyer.hashltol(copy, HashChain.VALUE_LENGTH);
            gap--;
        }

        if (!Arrays.equals(copy, ducklingValue)) {
            return false;
        }
        ducklingValue = value;
        ducklingPosition = pos;

        if (frame.bytes.remaining() == 1 + HashChain.VALUE_LENGTH) {
            gap = duckling.hashTree.pairChain.currentPosition - frame.bytes.get();
            if (gap < 0) {
                return false;
            }
            value = new byte[HashChain.VALUE_LENGTH];
            for (i = 0; i < HashChain.VALUE_LENGTH; i++) {
                value[i] = frame.bytes.get();
            }

            copy = Arrays.copyOf(duckling.hashTree.pairChain.currentValue, HashChain.VALUE_LENGTH);
            while (gap != 0) {
                copy = daviesMeyer.hashltol(copy, HashChain.VALUE_LENGTH);
                gap--;
            }

            if (!Arrays.equals(copy, value)) {
                return false;
            }

            duckling.setImprintable(true);
            return true;
        }

        return !frame.bytes.hasRemaining();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ByteBuffer bb;

        if ((event.getAction() != MotionEvent.ACTION_DOWN)
            && (event.getAction() != MotionEvent.ACTION_UP)) {
          return false;
        }

        if (duckling.hashTree == null) {
            duckling.hashTree = new HashTree();
        }

        bb = ByteBuffer.allocate(1 + HashChain.VALUE_LENGTH);
        bb.put((byte) duckling.hashTree.pairChain.currentPosition);
        bb.put(duckling.hashTree.pairChain.currentValue);

        if (!duckling.hashTree.pairChain.next()) {
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            duckling.motherDuck.sendCommandFrame(Ieee154Constants.PRESS_IDENTIFIER, duckling, bb.array());
        } else {
            duckling.motherDuck.sendCommandFrame(Ieee154Constants.RELEASE_IDENTIFIER, duckling, bb.array());
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        duckling.showProgress();
        duckling.setState(new BindingState(duckling, ducklingValue, ducklingPosition));
    }
}

package de.hhi.sixdoku.services.crypt;

import java.security.SecureRandom;

public class HashTree {

    public HashChain pairChain;
    public HashChain pingChain;
    private DaviesMeyer daviesMeyer;

    public HashTree() {
        byte[] pingRoot;

        daviesMeyer = new DaviesMeyer();
        pingRoot = new byte[HashChain.VALUE_LENGTH];
        new SecureRandom().nextBytes(pingRoot);
        pingChain = new HashChain(pingRoot);
        pairChain = new HashChain(daviesMeyer.hashltol(pingChain.currentValue, HashChain.VALUE_LENGTH));
    }
}

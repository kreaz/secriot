package de.hhi.sixdoku.services.channels;

import java.util.Iterator;

public class YaoChannelIterator implements Iterator<Integer> {

    private EnergyScanResult energyScanResult;
    private Integer current;
    private int left;

    public YaoChannelIterator(EnergyScanResult energyScanResult) {
        this.energyScanResult = energyScanResult;
        current = 26;
        left = energyScanResult.size();
    }

    @Override
    public boolean hasNext() {
        return left != 0;
    }

    @Override
    public Integer next() {
        do {
            current = ((current - 11 + 7) % 16) + 11;
        } while (energyScanResult.get(current) == null);
        left--;
        return current;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

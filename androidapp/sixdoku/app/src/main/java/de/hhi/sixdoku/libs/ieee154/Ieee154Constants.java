package de.hhi.sixdoku.libs.ieee154;

public interface Ieee154Constants {

    public static final byte PRESSED_IDENTIFIER = 0x20;
    public static final byte RELEASED_IDENTIFIER = 0x21;
    public static final byte PRESS_IDENTIFIER = 0x22;
    public static final byte RELEASE_IDENTIFIER = 0x23;
    public static final byte BIND_IDENTIFIER = 0x24;
    public static final byte BOUND_IDENTIFIER = 0x25;
    public static final byte PING_IDENTIFIER = 0x26;
    public static final byte PONG_IDENTIFIER = 0x27;

    /**
     * Used to send RSSIs to Bob
     */
    public static final byte DEBUG1_IDENTIFIER = 0x28;
    public static final byte DEBUG2_IDENTIFIER = 0x29;
}

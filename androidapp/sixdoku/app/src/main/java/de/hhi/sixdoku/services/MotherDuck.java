package de.hhi.sixdoku.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import java.nio.ByteBuffer;
import java.util.List;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.libs.ieee154.Ieee154Constants;
import de.hhi.sixdoku.services.channels.EnergyScanResult;
import de.hhi.sixdoku.services.dongle.Dongle;
import de.hhi.sixdoku.services.dongle.DongleBinder;
import de.hhi.sixdoku.services.dongle.DongleInputObserver;
import de.hhi.sixdoku.ui.MainActivity;
import de.hhi.sixdoku.ui.ducklings.Duckling;
import de.hhi.sixdoku.ui.ducklings.DucklingsAdapter;

public class MotherDuck extends Service implements ServiceConnection, DongleInputObserver {

    private boolean running;
    private MotherDuckBinder binder;
    private Dongle dongle;
    public DucklingsAdapter ducklingsAdapter;
    private Handler handler;
    private Duckling pingingDuckling;

    ///////////////////////// LIFECYCLE //////////////////////////

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!running) {
            running = true;
            binder = new MotherDuckBinder(this);
            ducklingsAdapter = new DucklingsAdapter(getApplicationContext());
            startDongleService();
            bindService(new Intent(this, Dongle.class), this, Context.BIND_AUTO_CREATE);
            handler = new Handler(Looper.getMainLooper());
            Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onStartCommand");
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (running) {
            running = false;
            unbindService(this);
        }

        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onDestroy");
    }

    ///////////////////////// Dongle //////////////////////////

    public void startDongleService() {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#startDongleService");

        startService(new Intent(this, Dongle.class));
    }

    public void stopDongleService() {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#stopDongleService");

        stopService(new Intent(this, Dongle.class));
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        dongle = ((DongleBinder) binder).dongle;
        dongle.registerObserver(this);
        onUsbChange();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        dongle.unregisterObserver();
        dongle = null;
    }

    public void onFrame(final Frame frame) {
        if (!frame.isCommandFrame()) {
            return;
        }

        handler.post(new Runnable() {

            @Override
            public void run() {
                List<Duckling> ducklings;
                boolean authentic;

                ducklings = ducklingsAdapter.get(frame.source);
                if (ducklings.isEmpty()) {
                    if (frame.commandFrameIdentifier != Ieee154Constants.PRESSED_IDENTIFIER) {
                        return;
                    }

                    ducklingsAdapter.add(new Duckling(frame.source, MotherDuck.this));
                } else {
                    authentic = false;
                    for (Duckling duckling : ducklings) {
                        if (duckling.onCommandFrame(frame)) {
                            authentic = true;
                            break;
                        }
                    }

                    if (!authentic
                            && (frame.commandFrameIdentifier == Ieee154Constants.PRESSED_IDENTIFIER)) {
                        ducklingsAdapter.add(new Duckling(frame.source, MotherDuck.this));
                    }
                }
            }
        });
    }

    @Override
    public void onPong(ByteBuffer rssis) {
        pingingDuckling.onPong(rssis);
        pingingDuckling = null;
    }

    @Override
    public void onError(Throwable e) {
        Log.w(MainActivity.TAG, this.getClass().getSimpleName() + "#onError", e);

        clearDucklings();
        dongle.disconnect();
        onUsbChange();
    }

    public void onUsbChange() {
        if (dongle != null) {
            dongle.onUsbChange();
            ducklingsAdapter.onUsbChange();
            if (!dongle.hasUsbDevice()) {
                pingingDuckling = null;
            }
        }
    }

    public boolean isDongleAttached() {
        return (dongle != null) && dongle.hasUsbDevice();
    }

    @Override
    public void onAddress(byte[] address) {

    }

    ///////////////////////// Ducklings //////////////////////////

    public void clearDucklings() {
        handler.post((new Runnable() {

            @Override
            public void run() {
                while (ducklingsAdapter.getCount() != 0) {
                    ducklingsAdapter.getItem(0).remove();
                }
            }
        }));
    }

    public void sendCommandFrame(byte commandFrameIdentifier, Duckling duckling, byte[] payload) {
        dongle.sendCommandFrame(commandFrameIdentifier, duckling.address, payload);
    }

    public void setChannel(byte channel) {
        dongle.setChannel(channel);
    }

    public void ping(byte[] pingChainRoot, byte[] pongChainEnding, Duckling duckling) {
        if (pingingDuckling == null) {
            pingingDuckling = duckling;
            dongle.ping(pingChainRoot, pongChainEnding, duckling.address);
        }
    }

    public boolean isPinging() {
        return pingingDuckling != null;
    }
}

package de.hhi.sixdoku.services.dongle;

import android.os.Binder;

public class DongleBinder extends Binder {

    public Dongle dongle;

    public DongleBinder(Dongle dongle) {
        this.dongle = dongle;
    }
}

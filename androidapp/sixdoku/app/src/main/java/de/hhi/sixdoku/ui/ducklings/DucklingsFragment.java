package de.hhi.sixdoku.ui.ducklings;

import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import de.hhi.sixdoku.R;
import de.hhi.sixdoku.services.MotherDuck;
import de.hhi.sixdoku.services.MotherDuckBinder;
import de.hhi.sixdoku.ui.MainActivity;

public class DucklingsFragment extends ListFragment implements ServiceConnection {

    private DucklingsAdapter ducklingsAdapter;
    private MotherDuck motherDuck;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().bindService(new Intent(getActivity(), MotherDuck.class), this, Context.BIND_AUTO_CREATE);
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onCreate");
    }

    public void updateEmptyText() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!motherDuck.isDongleAttached()) {
                    setEmptyText(getResources().getString(R.string.no_dungle));
                } else {
                    setEmptyText(getResources().getString(R.string.no_ducklings));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        getActivity().unbindService(this);
        super.onDestroy();
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onDestroy");
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onServiceConnected");
        motherDuck = ((MotherDuckBinder) binder).motherDuck;
        ducklingsAdapter = motherDuck.ducklingsAdapter;
        setListAdapter(ducklingsAdapter);
        ducklingsAdapter.activity = getActivity();
        ducklingsAdapter.ducklingsFragment = this;
        updateEmptyText();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onServiceDisconnected");
        ducklingsAdapter.ducklingsFragment = null;
        ducklingsAdapter.activity = null;
        ducklingsAdapter = null;
        setListAdapter(null);
    }
}

package de.hhi.sixdoku.ui.ducklings;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.util.Arrays;

import de.hhi.sixdoku.R;
import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.services.MotherDuck;
import de.hhi.sixdoku.services.channels.EnergyScanResult;
import de.hhi.sixdoku.services.crypt.HashTree;
import de.hhi.sixdoku.ui.ducklings.states.DucklingState;
import de.hhi.sixdoku.ui.ducklings.states.PairingState;

public class Duckling implements View.OnTouchListener, View.OnClickListener {

    public byte[] address;
    public MotherDuck motherDuck;
    private View row;
    private boolean highlighted;
    private Button imprintButton;
    private int imprintButtonVisibility;
    private ProgressBar progressBar;
    private int progressBarVisibility;
    private DucklingState state;
    private TextView ducklingInfo;
    private int position;
    public HashTree hashTree;

    public Duckling(byte[] address, MotherDuck motherDuck) {
        this.address = address;
        this.motherDuck = motherDuck;
        state = new PairingState(this);
        state.enter();
    }

    public void setState(DucklingState state) {
        this.state.leave();
        this.state = state;
        state.enter();
        updateProgress();
    }

    public void setRow(View row) {
        this.row = row;
        row.setOnTouchListener(this);
        imprintButton = (Button) row.findViewById(R.id.button_imprint);
        imprintButton.setOnClickListener(this);
        progressBar = (ProgressBar) row.findViewById(R.id.progress_bar);
        ducklingInfo = (TextView) row.findViewById(R.id.duckling_info);
        updateProgress();

        if (highlighted) {
            highlight();
        } else {
            unhighlight();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb;
        int i;

        sb = new StringBuilder(address.length * 2);
        for (i = 0; i < address.length; i++) {
            sb.append(String.format("%02x", address[i]));
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (!(that instanceof Duckling)) {
            return false;
        } else {
            return Arrays.equals(address, ((Duckling) that).address);
        }
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public void highlight() {
        highlighted = true;

        if (row != null) {
            row.setBackgroundColor(android.graphics.Color.BLACK);
            ducklingInfo.setTextColor(android.graphics.Color.WHITE);
        }
    }

    public void unhighlight() {
        highlighted = false;

        if (row != null) {
            row.setBackgroundColor(android.graphics.Color.WHITE);
            ducklingInfo.setTextColor(android.graphics.Color.BLACK);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        return state.onTouch(view, event);
    }

    public void setImprintable(boolean imprintable) {
        if (imprintable) {
            imprintButtonVisibility = View.VISIBLE;
        } else {
            imprintButtonVisibility = View.INVISIBLE;
            progressBarVisibility = View.GONE;
        }
        updateProgress();
    }

    public void showProgress() {
        imprintButtonVisibility = View.INVISIBLE;
        progressBarVisibility = View.VISIBLE;
        updateProgress();
    }

    @Override
    public void onClick(View view) {
        state.onClick(view);
    }

    public void updateProgress() {
        if (row != null) {
            imprintButton.setVisibility(imprintButtonVisibility);
            progressBar.setVisibility(progressBarVisibility);
            progressBar.setProgress(state.index);
            ducklingInfo.setText(motherDuck.getResources().getStringArray(R.array.preloading_phases)[state.index]);
        }
    }

    public boolean onCommandFrame(Frame frame) {
        return state.onCommandFrame(frame);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void remove() {
        state.leave();
        motherDuck.ducklingsAdapter.remove(this);
    }

    public void onPong(ByteBuffer rssis) {
        state.onPong(rssis);
    }
}

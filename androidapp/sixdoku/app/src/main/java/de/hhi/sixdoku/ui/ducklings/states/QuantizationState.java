package de.hhi.sixdoku.ui.ducklings.states;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.ui.ducklings.Duckling;

public class QuantizationState extends DucklingState {

    public QuantizationState(Duckling duckling) {
        super(duckling, 4);
    }

    @Override
    public boolean onCommandFrame(Frame frame) {
        return false;
    }
}

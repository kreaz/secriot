package de.hhi.sixdoku.services.crypt;

import javax.crypto.SecretKey;

/**
 * 128-bit key for use with AES-128
 */
public class Aes128Key implements SecretKey {

    private final byte[] bytes;

    public Aes128Key(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public String getAlgorithm() {
        return "AES";
    }

    @Override
    public String getFormat() {
        return "RAW";
    }

    @Override
    public byte[] getEncoded() {
        return bytes;
    }
}

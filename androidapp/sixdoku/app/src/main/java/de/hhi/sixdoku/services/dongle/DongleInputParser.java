package de.hhi.sixdoku.services.dongle;

import java.io.IOException;
import java.nio.ByteBuffer;

import de.hhi.sixdoku.libs.ieee154.FrameFactory;
import de.hhi.sixdoku.libs.usb.UsbDriver;
import de.hhi.sixdoku.services.channels.EnergyScanResult;
import de.hhi.sixdoku.ui.ducklings.states.SamplingState;

/**
 * Continuously dissects the USB input.
 */
public class DongleInputParser implements Runnable {

    private static final byte[] SNIF_PREAMBLE = {(byte) 0x53, (byte) 0x4E, (byte) 0x49, (byte) 0x46};
    private static final byte[] PONG_PREAMBLE = {(byte) 0x50, (byte) 0x4F, (byte) 0x4E, (byte) 0x47};
    private static final byte[] ADDR_PREAMBLE = {(byte) 0x41, (byte) 0x44, (byte) 0x44, (byte) 0x52};
    private DongleInputObserver dongleInputObserver;
    private UsbDriver usbDriver;
    private FrameFactory frameFactory;
    private Byte rssi;
    private ByteBuffer byteBuffer;
    private int snifState;
    private int pongState;
    private int addrState;
    private ByteBuffer address;
    private ByteBuffer rssis;

    public DongleInputParser(DongleInputObserver dongleInputListener, UsbDriver usbDriver, FrameFactory frameFactory) {
        this.dongleInputObserver = dongleInputListener;
        this.usbDriver = usbDriver;
        this.frameFactory = frameFactory;
    }

    public void parse() {
        usbDriver.doTask(this);
    }

    @Override
    public void run() {
        int bytesRead;
        byte[] dumpBuffer = new byte[64];

        try {
            if (usbDriver.isOpen()) {
                bytesRead = usbDriver.read(dumpBuffer, 0, dumpBuffer.length);
                if (bytesRead > 0) {
                    parse(dumpBuffer, bytesRead);
                }
                parse();
            }
        } catch (IOException e) {
            dongleInputObserver.onError(e);
        }
    }

    public void parse(byte[] bytes, int limit) {
        ByteBuffer bb = ByteBuffer.wrap(bytes, 0, limit);
        byte b;

        while (bb.hasRemaining()) {
            if (snifState == SNIF_PREAMBLE.length) {
                parseFrame(bb);
            } else if (pongState == PONG_PREAMBLE.length) {
                parsePong(bb);
            } else if (addrState == ADDR_PREAMBLE.length) {
                parseAddress(bb);
            } else {
                b = bb.get();

                if (b == SNIF_PREAMBLE[snifState]) {
                    snifState++;
                } else {
                    snifState = 0;
                }

                if (b == PONG_PREAMBLE[pongState]) {
                    pongState++;
                } else {
                    pongState = 0;
                }

                if (b == ADDR_PREAMBLE[addrState]) {
                    addrState++;
                } else {
                    addrState = 0;
                }
            }
        }
    }

    private void parseFrame(ByteBuffer bb) {
        while (bb.hasRemaining()) {
            if (rssi == null) {
                rssi = bb.get();
            } else if (byteBuffer == null) {
                byteBuffer = ByteBuffer.allocate(bb.get());
                byteBuffer.mark();
            } else {
                if (byteBuffer.position() == byteBuffer.limit()) {
                    break;
                }
                byteBuffer.put(bb.get());
            }
        }

        if ((byteBuffer != null) && (byteBuffer.position() == byteBuffer.limit())) {
            dongleInputObserver.onFrame(frameFactory.parse(byteBuffer, rssi));
            byteBuffer = null;
            rssi = null;
            snifState = 0;
        }
    }

    private void parsePong(ByteBuffer bb) {
        while (bb.hasRemaining()) {
            if (rssis == null) {
                rssis = ByteBuffer.allocate(SamplingState.MAX_PINGS);
                rssis.mark();
            } else {
                if (rssis.position() == rssis.limit()) {
                    break;
                }

                rssis.put(bb.get());
            }
        }

        if ((rssis != null) && (rssis.position() == rssis.limit())) {
            dongleInputObserver.onPong(rssis);
            pongState = 0;
            rssis = null;
        }
    }

    private void parseAddress(ByteBuffer bb) {
        while (bb.hasRemaining()) {
            if (address == null) {
                address = ByteBuffer.allocate(8);
                address.mark();
            } else {
                if (address.position() == address.limit()) {
                    break;
                }

                address.put(bb.get());
            }
        }

        if ((address != null) && (address.position() == address.limit())) {
            dongleInputObserver.onAddress(address.array());
            addrState = 0;
            address = null;
        }
    }
}

package de.hhi.sixdoku.services.crypt;

import android.util.Log;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import de.hhi.sixdoku.ui.MainActivity;

/**
 * AES-128 Block Cipher
 */
public class Aes128 {

    private Cipher aes;

    public Aes128() {
        try {
            aes = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException|NoSuchPaddingException e) {
            Log.e(MainActivity.TAG, this.getClass().getSimpleName() + "#Aes128", e);
        }
    }

    public byte[] encrypt(byte[] key, byte[] data) {
        try {
            aes.init(Cipher.ENCRYPT_MODE, new Aes128Key(key), new IvParameterSpec(new byte[16]));
            return aes.doFinal(data);
        } catch (InvalidKeyException|InvalidAlgorithmParameterException|BadPaddingException|IllegalBlockSizeException e) {
            Log.e(MainActivity.TAG, this.getClass().getSimpleName() + "#encrypt", e);
            return null;
        }
    }
}

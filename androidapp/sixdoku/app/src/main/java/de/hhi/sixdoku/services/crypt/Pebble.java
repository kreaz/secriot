package de.hhi.sixdoku.services.crypt;

import android.util.Log;

import de.hhi.sixdoku.ui.MainActivity;

public class Pebble {

    public int startIncr;
    public int destIncr;
    public int position;
    public int destination;
    public byte[] value;

    public void printPebble() {
        StringBuilder sb;

        sb = new StringBuilder(value.length * 2);

        for (int i = 0; i < value.length; i++) {
            sb.append(String.format("%02x", value[i]));
        }

        Log.d(MainActivity.TAG, "startIncr = " + startIncr + " destIncr = " + destIncr + " position = " + position + " destination = " + destination + " value = " + sb.toString());
    }
}

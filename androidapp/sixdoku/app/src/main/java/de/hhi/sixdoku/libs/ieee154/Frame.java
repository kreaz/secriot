package de.hhi.sixdoku.libs.ieee154;

import android.util.Log;

import java.nio.ByteBuffer;

import de.hhi.sixdoku.ui.MainActivity;

public class Frame {

    public ByteBuffer bytes;
    public int payloadPosition;
    public Byte rssi;

    public FrameType frameType;
    public boolean securityEnabled;
    public boolean framePending;
    public boolean ackRequested;
    public boolean panIdCompressed;
    public AddressingMode destinationAddressingMode;
    public AddressingMode sourceAddressingMode;
    public byte sequenceNumber;
    public byte[] destination;
    public byte[] source;
    public byte commandFrameIdentifier;

    public boolean isCommandFrame() {
        return frameType == FrameType.COMMAND;
    }

    public void print() {
        StringBuilder sb;

        bytes.reset();
        sb = new StringBuilder(bytes.remaining() * 2);

        while (bytes.hasRemaining()) {
            sb.append(String.format("%02x", bytes.get()));
        }
        Log.d(MainActivity.TAG, sb.toString());
    }

    public void printPayload() {
        StringBuilder sb;

        bytes.position(payloadPosition);
        sb = new StringBuilder(bytes.remaining() * 2);

        while (bytes.hasRemaining()) {
            sb.append(String.format("%02x", bytes.get()));
        }
        Log.d(MainActivity.TAG, sb.toString());
    }

    public boolean isBroadcast() {
        return (destinationAddressingMode == AddressingMode.SHORT)
                && (destination[0] == 0xFF)
                && (destination[1] == 0xFF);
    }
}

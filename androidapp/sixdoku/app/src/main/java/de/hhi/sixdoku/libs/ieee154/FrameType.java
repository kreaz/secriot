package de.hhi.sixdoku.libs.ieee154;

public enum FrameType {

    BEACON((byte) 0),
    DATA((byte) 1),
    ACKNOWLEDGEMENT((byte) 2),
    COMMAND((byte) 3);

    public byte value;

    private FrameType(byte value) {
        this.value = value;
    }

    public static FrameType getFrameType(byte value) {
        for (FrameType frameType : FrameType.values()) {
            if (frameType.value == value) {
                return frameType;
            }
        }
        return null;
    }
}

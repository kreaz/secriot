package de.hhi.sixdoku.libs.ieee154;

public enum AddressingMode {

    NOT_PRESENT((byte) 0, 0),
    SHORT((byte) 2, 2),
    EXTENDED((byte) 3, 8);

    public byte value;
    public int addressLength;

    private AddressingMode(byte value, int addressLength) {
        this.value = value;
        this.addressLength = addressLength;
    }

    public static AddressingMode get(byte value) {
        for (AddressingMode addressingMode : AddressingMode.values()) {
            if (addressingMode.value == value) {
                return addressingMode;
            }
        }
        return null;
    }
}

package de.hhi.sixdoku.ui.ducklings.states;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;

import java.nio.ByteBuffer;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.services.channels.EnergyScanResult;
import de.hhi.sixdoku.services.crypt.DaviesMeyer;
import de.hhi.sixdoku.ui.ducklings.Duckling;

public abstract class DucklingState extends BroadcastReceiver implements View.OnTouchListener, View.OnClickListener {

    protected Duckling duckling;
    public int index;
    protected DaviesMeyer daviesMeyer;

    public DucklingState(Duckling duckling, int index) {
        this.duckling = duckling;
        this.index = index;
        daviesMeyer = new DaviesMeyer();
    }

    public void enter() {

    }

    public void leave() {

    }

    /**
     * @return true <-> authentic
     */
    public abstract boolean onCommandFrame(Frame frame);

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        return false;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

    }

    public void onPong(ByteBuffer rssis) {

    }
}

package de.hhi.sixdoku.services.dongle;

import java.nio.ByteBuffer;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.services.channels.EnergyScanResult;

public interface DongleInputObserver {

    void onFrame(Frame frame);

    void onPong(ByteBuffer rssis);

    void onAddress(byte[] address);

    void onError(Throwable e);
}

package de.hhi.sixdoku.services.crypt;

public class DaviesMeyer {

    private Aes128 aes128;

    public DaviesMeyer() {
        aes128 = new Aes128();
    }

    public byte[] hash16to16(byte[] msg) {
        return aes128.encrypt(msg, new byte[16]);
    }

    public byte[] hashltol(byte[] msg, int l) {
        byte[] tempMsg = new byte[16];
        byte[] hash;
        byte[] tempHash = new byte[l];
        int i;

        for (i = 0; i < msg.length; i++) {
            tempMsg[i] = msg[i];
        }

        hash = hash16to16(tempMsg);

        for (i = 0; i < l; i++) {
            tempHash[i] = hash[i];
        }
        return tempHash;
    }
}

package de.hhi.sixdoku.services.dongle;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.nio.ByteBuffer;

import de.hhi.sixdoku.libs.ieee154.Frame;
import de.hhi.sixdoku.libs.ieee154.FrameFactory;
import de.hhi.sixdoku.libs.usb.TelosBDriver;
import de.hhi.sixdoku.libs.usb.UsbDriver;
import de.hhi.sixdoku.services.channels.EnergyScanResult;
import de.hhi.sixdoku.services.crypt.HashChain;
import de.hhi.sixdoku.ui.MainActivity;
import de.hhi.sixdoku.ui.ducklings.Duckling;

public class Dongle extends Service implements DongleInputObserver {

    public static final String ACTION_USB_PERMISSION = "de.hhi.sixdoku.background.ACTION_USB_PERMISSION";
    private boolean running;
    private UsbManager usbManager;
    private DongleBinder binder;
    private UsbDevice usbDevice;
    private UsbDriver usbDriver;
    private DongleInputObserver dongleInputObserver;
    public boolean isScanning;
    private FrameFactory frameFactory;
    public byte[] address;
    private Duckling pingingDuckling;

    ///////////////////////// LIFECYCLE //////////////////////////

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!running) {
            running = true;
            usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            binder = new DongleBinder(this);
            frameFactory = new FrameFactory(this);
            Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onStartCommand");
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (running) {
            running = false;
            disconnect();
        }

        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onDestroy");
    }

    //////////////////// USB ACTIVITY HANDLING ///////////////////

    public void onUsbChange() {
        UsbDevice newUsbDevice;

        newUsbDevice = null;
        for (UsbDevice device : usbManager.getDeviceList().values()) {
            if ((device.getProductId() == TelosBDriver.PRODUCT_ID) && (device.getVendorId() == TelosBDriver.VENDOR_ID)) {
                newUsbDevice = device;
                break;
            }
        }
        if ((newUsbDevice != null) && newUsbDevice.equals(usbDevice)) {
            return;
        }
        if ((newUsbDevice == null) && (usbDevice == null)) {
            return;
        }
        usbDevice = newUsbDevice;
        onUsbDeviceChanged();
    }

    private void onUsbDeviceChanged() {
        if (usbDevice == null) {
            disconnect();
            Toast.makeText(this, "Device Detached", Toast.LENGTH_SHORT).show();
        } else {
            if (!usbManager.hasPermission(usbDevice)) {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                usbManager.requestPermission(usbDevice, pendingIntent);
                return;
            }

            usbDriver = new TelosBDriver(usbDevice, usbManager);
            try {
                usbDriver.open();
                Toast.makeText(this, "Device Attached", Toast.LENGTH_SHORT).show();
                new DongleInputParser(this, usbDriver, frameFactory).parse();
                setChannel((byte) 26);
                retrieveAddress();
            } catch (IOException e) {
                disconnect();
            }
        }
    }

    public void disconnect() {
        if (usbDriver != null) {
            usbDriver.close();
        }
        usbDriver = null;
        usbDevice = null;
    }

    //////////////////////////// USB /////////////////////////////

    public void sendCommandFrame(byte commandFrameIdentifier, byte[] destination, byte[] payload) {
        if (address != null) {
            Frame frame = frameFactory.createCommandFrame(destination, commandFrameIdentifier, payload);
            final ByteBuffer bb = ByteBuffer.allocate(256);

            bb.mark();
            bb.put((byte) 0x00);
            bb.put((byte) frame.bytes.limit());
            frame.bytes.reset();
            bb.put(frame.bytes);
            bb.put((byte) 0x0A);
            bb.limit(bb.position());

            usbDriver.doTask(new Runnable() {

                @Override
                public void run() {
                    try {
                        usbDriver.write(bb.array(), 0, bb.limit());
                    } catch (IOException e) {
                        Log.e(MainActivity.TAG, this.getClass().getSimpleName() + "#sendCommandFrame", e);
                    }
                }
            });
        } else {
            retrieveAddress();
        }
    }

    public void setChannel(byte channel) {
        final ByteBuffer bb = ByteBuffer.allocate(3);

        bb.put((byte) 0x01);
        bb.put(channel);
        bb.put((byte) 0x0A);

        usbDriver.doTask(new Runnable() {

            @Override
            public void run() {
                try {
                    usbDriver.write(bb.array());
                } catch (IOException e) {
                    Log.e(MainActivity.TAG, this.getClass().getSimpleName() + "#setChannel", e);
                }
            }
        });
    }

    public void ping(byte[] pingChainRoot, byte[] pongChainEnding, byte[] ducklingAddress) {
        final ByteBuffer bb = ByteBuffer.allocate(2 + HashChain.VALUE_LENGTH + HashChain.VALUE_LENGTH + 8);

        bb.put((byte) 0x02);
        bb.put(pingChainRoot);
        bb.put(pongChainEnding);
        bb.put(ducklingAddress);
        bb.put((byte) 0x0A);

        usbDriver.doTask(new Runnable() {

            @Override
            public void run() {
                try {
                    usbDriver.write(bb.array());
                } catch (IOException e) {
                    Log.e(MainActivity.TAG, this.getClass().getSimpleName() + "#ping", e);
                }
            }
        });
    }

    public void retrieveAddress() {
        usbDriver.doTask(new Runnable() {

            @Override
            public void run() {
                try {
                    usbDriver.write(new byte[] { (byte) 0x03 , (byte) 0x0A });
                } catch (IOException e) {
                    Log.e(MainActivity.TAG, this.getClass().getSimpleName() + "#getAddress", e);
                }
            }
        });
    }

    public boolean hasUsbDevice() {
        return usbDevice != null;
    }

    ////////////////// DongleInputObserver ///////////////////

    public void registerObserver(DongleInputObserver dongleInputObserver) {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#registerObserver");

        this.dongleInputObserver = dongleInputObserver;
    }

    public void unregisterObserver() {
        dongleInputObserver = null;
    }

    @Override
    public void onFrame(Frame frame) {
        if (dongleInputObserver != null) {
            dongleInputObserver.onFrame(frame);
        }
    }

    @Override
    public void onPong(ByteBuffer rssis) {
        if (dongleInputObserver != null) {
            dongleInputObserver.onPong(rssis);
        }
    }

    @Override
    public void onAddress(byte[] address) {
        Log.d(MainActivity.TAG, this.getClass().getSimpleName() + "#onAddress");

        this.address = address;
        if (dongleInputObserver != null) {
            dongleInputObserver.onAddress(address);
        }
    }

    @Override
    public void onError(Throwable e) {
        if (dongleInputObserver != null) {
            dongleInputObserver.onError(e);
        }
    }
}

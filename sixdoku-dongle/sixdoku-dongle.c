/*
 * Copyright (c) 2014, Fraunhofer Heinrich-Hertz-Institut.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "contiki.h"
#include "netstack.h"
#include "net/llsec/command-broker.h"
#include "net/packetbuf.h"
#include "net/linkaddr.h"
#include "dev/serial-line.h"
#include "sys/ctimer.h"
#include "lib/uhash-chain.h"
#include "lib/uhash.h"
#include <stdio.h>

#define PING_IDENTIFIER     0x26
#define PONG_IDENTIFIER     0x27
#define MAX_PINGS           64
#define TIMEOUT             (CLOCK_SECOND/2)
#define PING_DELAY          (CLOCK_SECOND/80)
#define INVALID_RSSI        127

static void on_timeout(void *ptr);
static void next_phase(void *ptr);
static void send_ping(void);
static void on_sampling_done(void);

static const char *SNIF_PREAMBLE = "SNIF";
static const char *ADDR_PREAMBLE = "ADDR";
static const char *PONG_PREAMBLE = "PONG";
static uint8_t current_channel;
static int8_t phase;
static int8_t rssis[MAX_PINGS];
static struct ctimer timeout;
static uhash_chain_t ping_chain;
static linkaddr_t duckling_addr;
static uint8_t duckling_pong_position;
static uint8_t duckling_pong_value[UHASH_CHAIN_VALUE_LEN];

PROCESS(sixdoku_dongle_process, "Sixdoku dongle process");
AUTOSTART_PROCESSES(&sixdoku_dongle_process);

/*---------------------------------------------------------------------------*/
static uint8_t *
prepare_command(uint8_t command_frame_id)
{
  uint8_t *payload;
  
  packetbuf_clear();
  packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_CMDFRAME);
  payload = packetbuf_dataptr();
  payload[0] = command_frame_id;
  packetbuf_set_datalen(1);
  
  return payload + 1;
}
/*---------------------------------------------------------------------------*/ 
static void
set_radio(uint8_t channel, int8_t dbm)
{
  NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, channel);
  NETSTACK_RADIO.set_value(RADIO_PARAM_TXPOWER, dbm);
}
/*---------------------------------------------------------------------------*/
static void
hop_channel(void)
{
  current_channel = ((current_channel - 11 + 7) % 16) + 11;
}
/*---------------------------------------------------------------------------*/
static void
on_timeout(void *ptr)
{
  rssis[phase] = INVALID_RSSI;
  next_phase(&timeout);
}
/*---------------------------------------------------------------------------*/
static void
next_phase(void *ptr)
{
  if(++phase == MAX_PINGS) {
    on_sampling_done();
    return;
  }
  
  if(ptr) {
    ctimer_reset(&timeout);
  } else {
    ctimer_stop(&timeout);
    ctimer_set(&timeout, TIMEOUT, on_timeout, NULL);
  }
  
  hop_channel();
  uhash_chain_next(&ping_chain);
  set_radio(current_channel, -15);

  if(phase) {
    /* Gives the PONGer time for switching the channel */
    clock_wait(PING_DELAY);
  }
  send_ping();
}
/*---------------------------------------------------------------------------*/
static void
send_ping(void)
{
  uint8_t *payload;

  packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, &duckling_addr);
  payload = prepare_command(PING_IDENTIFIER);
  memcpy(payload, ping_chain.current_value, UHASH_CHAIN_VALUE_LEN);
  packetbuf_set_datalen(1 + UHASH_CHAIN_VALUE_LEN);
  NETSTACK_MAC.send(NULL, NULL);
}
/*---------------------------------------------------------------------------*/
static void
on_pong(void)
{
  uint8_t *payload;
  uint8_t value[UHASH_CHAIN_VALUE_LEN];
  uint8_t gap;
  uint8_t pos;
  
  payload = packetbuf_dataptr();
  pos = phase + 1;
  memcpy(value, payload + 1, UHASH_CHAIN_VALUE_LEN);
  gap = pos - duckling_pong_position;
  while(gap--) {
    uhash_l_to_l(value, value, UHASH_CHAIN_VALUE_LEN);
  }
  
  if(memcmp(value, duckling_pong_value, UHASH_CHAIN_VALUE_LEN)) {
    return;
  }

  memcpy(duckling_pong_value, payload + 1, UHASH_CHAIN_VALUE_LEN);
  duckling_pong_position = pos;
  rssis[phase] = packetbuf_attr(PACKETBUF_ATTR_RSSI);
  next_phase(NULL);
}
/*---------------------------------------------------------------------------*/
static void
on_sampling_done(void)
{
  uint8_t i;
  
  ctimer_stop(&timeout);
  set_radio(26, 0);
  current_channel = 26;
  
  printf("%s", PONG_PREAMBLE);
  for (i = 0; i < MAX_PINGS; i++) {
    putchar(rssis[i]);
  }
}
/*---------------------------------------------------------------------------*/
static void
input()
{
  uint8_t rssi;
  uint8_t totlen;
  uint8_t *hdrptr; 
  uint8_t i;
  
  rssi = (uint8_t) packetbuf_attr(PACKETBUF_ATTR_RSSI);
  totlen = packetbuf_totlen();
  hdrptr = packetbuf_hdrptr();

  printf("%s%c%c", SNIF_PREAMBLE, rssi, totlen);
  for (i = 0; i < totlen; i++) {
    putchar(hdrptr[i]);
  }
}
/*---------------------------------------------------------------------------*/
static void
init(void)
{
  command_broker_subscribe(PONG_IDENTIFIER, on_pong);
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sixdoku_dongle_process, event, data)
{
  uint8_t *in;
  uint8_t i;
  uint8_t offset;
  uint8_t *payload;
  
  PROCESS_BEGIN();
  
  while(1) {
    PROCESS_WAIT_EVENT_UNTIL(event == serial_line_event_message && data != NULL);
    
    in = data;
    i = 0;
    offset = 0;
    while(in[++i + offset] != 0x0A) {
      if(in[i + offset] == 0x00) {
        offset++;
        if(in[i + offset] == 0x00) {
          /* zero */
          in[i] = 0;
        } else if(in[i + offset] == 0x01) {
          /* line feed */
          in[i] = 0x0A;
        } else {
          /* carriage return */
          in[i] = 0x0D;
        }
      } else {
        in[i] = in[i + offset];
      }
    }
    
    switch(in[0]) {
    case 0x00:
      packetbuf_clear();
      payload = packetbuf_dataptr();
      memcpy(payload, in + 2, in[1]);
      NETSTACK_RADIO.send(payload, in[1]);
      break;
    case 0x01:
      NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, in[1]);
      break;
    case 0x02:
      in++;
      memcpy(ping_chain.current_value, in, UHASH_CHAIN_VALUE_LEN);
      in += UHASH_CHAIN_VALUE_LEN;
      memcpy(duckling_pong_value, in, UHASH_CHAIN_VALUE_LEN);
      in += UHASH_CHAIN_VALUE_LEN;
      memcpy(&duckling_addr, in, LINKADDR_SIZE);
      
      uhash_chain_init(&ping_chain);
      duckling_pong_position = 0;
      phase = -1;
      current_channel = 26;
      
      next_phase(NULL);
      break;
    case 0x03:
      printf("%s", ADDR_PREAMBLE);
      for (i = 0; i < 8; i++) {
        putchar(linkaddr_node_addr.u8[i]);
      }
      break;
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
const struct network_driver sixdoku_dongle_driver = {
  "sixdoku_sniffer_driver",
  init,
  input
};
/*---------------------------------------------------------------------------*/
